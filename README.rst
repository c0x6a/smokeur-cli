===========
Smokeur CLI
===========

`Smokeur <https://gitlab.com/c0x6a/smokeur>`_ CLI to upload files from
your computer.

How to install and run
----------------------

Simply pip install it:

.. code-block:: shell

   pip install smokeur-cli


Configure environment variables and run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You have to set the ``SMOKEUR_SERVER`` environment variable before using
this tool.

then you can run it from a terminal:

.. code-block:: shell

   $ smokeur /path/to/a/file.jpg


you can upload any kind of files.
